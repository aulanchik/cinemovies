# CineMovies
[![Netlify Status](https://api.netlify.com/api/v1/badges/445c4eca-594f-4c58-96cf-13be6403b7b9/deploy-status)](https://app.netlify.com/sites/cinemovies/deploys)

Attempt to recreate popular multimedia streaming platform using ReactJS and Firebase for the educational purposes. All images, copyrights, logos and trademarks belongs to their respective owners.

## Live version

Live version of this project is deployed on Netlify platform and can be found [here](http://cinemovies.netlify.com)