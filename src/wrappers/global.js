import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
    html, body {
        font-family: 'Helvetica Neue', Arial, sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        background-color: #000000;
        color: #333333;
        font-size: 16px;
        margin: 0;
    }
`;

export default GlobalStyle;