import React from "react";
import { Link as RouterLink } from "react-router-dom";
import * as Style from "./styles/header";

const Header = ({ background = true, children, ...props }) => {
  return background ? (
    <Style.Background {...props}>{children}</Style.Background>
  ) : (
    children
  );
};

Header.Frame = ({ children, ...props }) => {
  return <Style.Container {...props}>{children}</Style.Container>;
};

Header.Button = ({ location, children }) => {
  return <Style.Button to={location}>{children}</Style.Button>;
};

Header.Logo = ({ location, ...props }) => {
  return <RouterLink to={location}></RouterLink>;
};

Header.Group = ({ children, ...props }) => {
  return <Style.Group {...props}>{children}</Style.Group>;
};

Header.Text = ({ children, ...props }) => {
  return <Style.Text {...props}>{children}</Style.Text>;
};

Header.Link = ({ children, ...props }) => {
  return <Style.Link {...props}>{children}</Style.Link>;
};

Header.Feature = ({ children, ...props }) => {
  return <Style.Feature {...props}>{children}</Style.Feature>;
};

Header.FeaturePromo = ({ children, ...props }) => {
  return <Style.FeaturePromo {...props}>{children}</Style.FeaturePromo>;
};

Header.PlayButton = ({children, ...props}) => {
  return <Style.PlayButton>{children}</Style.PlayButton>
}

export default Header;
