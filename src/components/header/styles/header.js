import styled from "styled-components";
import { Link as RouterLink } from "react-router-dom";

const Background = styled.section`
  display: flex;
  flex-direction: column;
  background: url(${({ src }) => (src ? `../images/misc/${src}.jpg` : '../images/misc/home-bg.jpg')}) top left / cover no-repeat;

  @media (max-width: 1100px) {
    ${({ dontShowOnSmallViewPort }) =>
      dontShowOnSmallViewPort && `background: none;`}
  }
`;

const Container = styled.div`
  display: flex;
  margin: 0 56px;
  height: 64px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  a {
    display: flex;
  }

  @media (max-width: 1000px) {
    margin: 0 30px;
  }
`;

const Link = styled.p`
  color: #fff;
  text-decoration: none;
  margin-right: 30px;
  font-weight: ${({ active }) => (active === "true" ? "700" : "normal")};
  cursor: pointer;

  &:hover {
    font-weight: bold;
  }
  &:last-of-type {
    margin-right: 0;
  }
`;

const Button = styled(RouterLink)`
  display: flex;
  jusitfy-content: space-around;
  background-color: #e50914;
  width: 84px;
  height: fit-content;
  color: white;
  border: 0;
  font-size: 15px;
  border-radius: 3px;
  padding: 8px 17px;
  cursor: pointer;
  text-decoration: none;
  box-sizing: border-box;

  &:hover {
    background: #f40612;
  }
`;

const Logo = styled.div`
  height: 30px;
  width: 108px;
  text-transform: uppercase;
  text-decoration: none;

  @media (min-width: 1200px) {
    height: 45px;
    width: 167px;
  }
`;

const Group = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.p`
  color: white;
  font-size: 22px;
  line-height: normal;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.45);
`;

const Feature = styled(Container)`
  display: flex;
  padding: 175px 0 500px 0;
  flex-direction: column;
  align-items: flex-start;
  width: 45%;

  @media (max-width: 1100px) {
    display: none;
  }
`;

const FeaturePromo = styled.h2`
  display: flex;
  color: #ffffff;
  font-size: 50px;
  line-height: normal;
  font-weight: bold;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.45);
  margin: 0;
`;

const PlayButton = styled.button`
    box-shadow: 0 0.6vw 1vw -0.4vw rgba(0, 0, 0, 0.35);
    background-color: #e6e6e6;
    color: #000;
    border-width: 0;
    padding: 10px 20px;
    border-radius: 5px;
    width: 130px;
    max-width: 130px;
    font-weight: bold;
    font-size: 20px;
    margin-top: 10px;
    cursor: pointer;
    transition: background-color: 0.5s ease;
    
    &:hover {
        background: #ff1e1e;
        color: white;
    }
`;

export {
  Background,
  Container,
  Button,
  Link,
  Logo,
  Group,
  Text,
  Feature,
  FeaturePromo,
  PlayButton,
};
