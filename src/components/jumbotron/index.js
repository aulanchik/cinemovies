import React from 'react';
import * as Style from './styles/jumbotron';

const Jumbotron = ({ children, direction='row', ...props }) => {
    return (
        <Style.Item {...props}>
            <Style.Inner direction={direction}>
                {children}
            </Style.Inner>
        </Style.Item>
    )
}

Jumbotron.Container = ({ children, ...props }) => {
    return <Style.Container {...props}>{children}</Style.Container>;
}

Jumbotron.Title = ({ children, ...props }) => {
    return <Style.Title {...props}>{children}</Style.Title>;
}

Jumbotron.SubTitle = ({ children, ...props }) => {
    return <Style.SubTitle {...props}>{children}</Style.SubTitle>;
}

Jumbotron.Image = ({...props}) => {
    return <Style.Image {...props} />;
}

Jumbotron.Pane = ({children, ...props}) => {
    return <Style.Pane {...props}>{children}</Style.Pane>
}

Jumbotron.Item = ({children, ...props}) => {
    return <Style.Item {...props}>{children}</Style.Item>
}

export default Jumbotron;