import styled from 'styled-components';

const Container = styled.section`
    display: flex;
    padding-bottom: 40px;
`;

const Frame = styled.div`
    width: 100%;
    padding-bottom: 25px;
`

const Inner = styled.div`
    display: flex;
    padding: 70px 45px;
    flex-direction: column;
    max-width: 800px;
    width: 100%;
    margin: auto;
`;

const Item = styled.div`
    color: white;
    margin-bottom: 10px;
    max-width: 800px;
    
    &:first-of-type {
        margin-top: 3em;
    }
`;

const Title = styled.h1`
    font-size: 50px;
    line-height: 1.1;
    text-align: center;
    color: white;
    
    @media (max-width: 600px) {
        font-size: 35px;
    }
`;

const Header = styled.div`
    display: flex;
    justify-content: space-between;
    cursor: pointer;
    box-sizing: border-box;
    margin-bottom: 1px;
    font-size: 26px;
    font-weight: normal;
    background: #303030;
    padding: 0.8em 1.2em 0.8em 1.2em;
    user-select: none;
    align-items: center;
    width: 100%;
    
    img {
        filter: brightness(0) invert(1);
        width: 24px;
        
        @media (max-width: 600px) {
            width: 16px;
        }
    }
    
    @media (max-width: 600px) {
        font-size: 16px;
    }
`;

const Body = styled.div`
    transition: max-height 0.25s cubic-bezier(0.5, 0, 0.1, 1);
    font-size: 26px;
    font-weight: normal;
    line-height: normal;
    background: #303030;
    padding: 0.8em 1.2em 0.8em 1.2em;
    user-select: none;
    align-items: center;
    text-align: justify;
    
    @media (max-width: 600px) {
        font-size: 16px;
        line-height: 22px;
    }
`;

export {
    Container,
    Frame,
    Inner,
    Item,
    Title,
    Header,
    Body
}