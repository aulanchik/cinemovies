import React from 'react';
import * as Style from './styles/accordion'
import ToggleContext from '../../contexts/ToggleContext';

const Accordion = ({children, ...props}) => {
    return (
        <Style.Container {...props}>
            <Style.Inner>{children}</Style.Inner>
        </Style.Container>
    )
}

Accordion.Frame = ({ children, ...props }) => {
    return <Style.Frame {...props}>{children}</Style.Frame>;
}

Accordion.Item = function AccordionItem({ children, ...props }) {
    const [toggleShow, setToggleShow] = React.useState(false);
    return (
        <ToggleContext.Provider value={{ toggleShow, setToggleShow }}>
            <Style.Item {...props}>{children}</Style.Item>
        </ToggleContext.Provider>
    )
}

Accordion.Title = ({ children, ...props }) => {
    return <Style.Title {...props}>{children}</Style.Title>;
}

Accordion.Header = function AccordionHeader({ children, ...props }) {
    const { toggleShow, setToggleShow } = React.useContext(ToggleContext);
    return (
        <Style.Header onClick={() => setToggleShow(!toggleShow)} {...props}>
            {children}
            {toggleShow ? (
                <img src="/images/icons/close-slim.png" alt="Close" />
            ) : (
                <img src="/images/icons/add.png" alt="Open" />
            )}
        </Style.Header>
    )
}

Accordion.Body = function AccordionBody({ children, ...props }) {
    const { toggleShow } = React.useContext(ToggleContext);
    return toggleShow ? <Style.Body {...props}>{children}</Style.Body> : null;
}

export default Accordion;