import React from 'react'
import * as Style from './styles/optform'

const OptForm = ({children, ...props}) => {
    return <Style.Container {...props}>{children}</Style.Container>
}

OptForm.Input = ({...props}) => {
    return <Style.Input {...props} />
}

OptForm.Text = ({children, ...props}) => {
    return <Style.Text {...props}>{children}</Style.Text>
}

OptForm.Button = ({children, ...props}) => {
    return <Style.Button {...props}>{children}</Style.Button>
}

OptForm.Break = () => {
    return <Style.Break />
}

export default OptForm;