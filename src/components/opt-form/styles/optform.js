import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    justify-content: center;
    height: 100%;
    margin-top: 20px;
    flex-wrap: wrap;

    @media (max-width: 1000px) {
        align-items: center;
    }
`;

const Input = styled.input`
    max-width: 450px;
    width: 100%;
    border: 0;
    padding: 10px;
    height: 70px;
    box-sizing: border-box;
    border-radius: 0px;
`;

const Text = styled.p`
    color: #ffffff;
    padding: 12px;
    line-height: 1.1;
    font-size: 1.25rem;
    text-align: center;
    font-weight: 400;
    margin: 0 auto;

    @media(max-width: 600px) {
        font-size: 16px;
        line-height: 22px;
    }
`;

const Button = styled.button`
    display: flex;
    align-items: center;
    height: 70px;
    background: #e50914;
    text-transform: uppercase;
    border-radius: 0px; 
    color: #ffffff;
    padding: 0 32px;
    font-size: 26px;
    cursor: pointer;
    border: none;

    &:hover {
        background: #f40612;
    }

    @media (max-width: 1000px) {
        height: 50px;
        font-size: 16px;
        margin-top: 20px;
        font-weight: bold;
    }
`;

const Break = styled.div`
    flex-basis: 100%;
    height: 100%;
`;

export {
    Break,
    Button,
    Text,
    Input,
    Container
}