import React from 'react'
import * as Style from './style/profiles';

const Profiles = ({ children, ...restProps }) => {
    return <Style.Container {...restProps}>{children}</Style.Container>;
}

Profiles.Title = function ProfilesTitle({ children, ...restProps }) {
    return <Style.Title {...restProps}>{children}</Style.Title>;
}

Profiles.List = function ProfilesList({ children, ...restProps }) {
    return <Style.List {...restProps}>{children}</Style.List>;
}

Profiles.User = function ProfilesUser({ children, ...restProps }) {
    return <Style.Item {...restProps}>{children}</Style.Item>;
}

Profiles.Picture = function ProfilesPicture({ src, ...restProps }) {
    return <Style.Picture {...restProps} src={src ? `/images/users/${src}.png` :
    '/images/misc/loading.gif'} />;
}

Profiles.Text = function ProfilesText({ children, ...restProps }) {
    return <Style.Text {...restProps}>{children}</Style.Text>;
}

export default Profiles;