import React from 'react'
import * as Style from './styles/form'

const Form = ({ children, ...props }) => {
    return <Style.Container {...props}>{children}</Style.Container>
}

Form.Title = ({ children, ...props }) => {
    return <Style.Title {...props}>{children}</Style.Title>
}

Form.SubTitle = ({ children, ...props }) => {
    return <Style.SubTitle {...props}>{children}</Style.SubTitle>
}

Form.Wrapper = ({ children, ...props }) => {
    return <Style.Wrapper {...props}>{children}</Style.Wrapper>
}

Form.Input = ({ children, ...props }) => {
    return <Style.Input {...props}>{children}</Style.Input>
}

Form.Link = ({children, ...props }) => {
    return <Style.Link {...props}>{children}</Style.Link>
}

Form.Submit = ({ children, ...props }) => {
    return <Style.Submit {...props}>{children}</Style.Submit>
}

Form.Error = ({ children, ...props }) => {
    return <Style.Error {...props}>{children}</Style.Error>
}

export default Form;