import styled from 'styled-components'
import { Link as RouterLink } from 'react-router-dom';

const Container = styled.section`
    display: flex;
    flex-direction: column;
    min-height: 450px;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.75);
    box-sizing: border-box;
    max-width: 450px;
    border-radius: 4px;
    margin: auto;
    width:100%;
    padding: 60px 70px 40px;
    margin-bottom: 90px;
`;

const Wrapper = styled.form`
    display: flex;
    flex-direction: column;
    max-width: 450px;
    width: 100%;
`;

const Title = styled.h1`
    color: #ffffff;
    font-size: 2.25rem;
    font-weight: 600;
    margin-bottom: 
`;

const SubTitle = styled.h2`
    margin-top: 10px;
    font-size: 13px;
    line-height: normal;
    color: #8c8c8c;
`;

const Input = styled.input`
    background: #333;
    border-radius: 4px;
    border: 0;
    color: #fff;
    height: 40px;
    line-height: 40px;
    padding: 5px 20px;
    margin-bottom: 20px;

    &:last-of-type {
        margin-bottom: 30px;
    }
`;


const Link = styled(RouterLink)`
    color: #ffffff;
    text-decoration: none;
    
    &:hover {
        text-decoration: underline;
    }
`;

const Error = styled.div`
    background: #e87c03;
    border-radius: 4px;
    font-size: 1.2rem;
    margin: 0 0 18px;
    color: #ffffff;
    padding: 15px 20px;
`;

const Submit = styled.button`
    background: #e50914;
    border-radius: 4px;
    font-weight: 700;
    font-size: 1rem;
    margin: 0 0 12px;
    padding: 16px;
    border: 0;
    color: #ffffff;
    cursor: pointer;

    &:disabled {
        opacity: 0.5;
        pointer-events: none;
    }

    &:hover {
        background: #f40612;
    }
`;

export {
    Container,
    Wrapper,
    Title,
    SubTitle,
    Input,
    Link,
    Submit,
    Error
}