import React from 'react'
import * as Style from './styles/feature'

const Feature = ({ children, ...props }) => {
    return <Style.Container {...props}>{children}</Style.Container>
}

Feature.Title = ({ children }) => {
    return <Style.Title>{children}</Style.Title>
}

Feature.SubTitle = ({ children }) => {
    return <Style.SubTitle>{children}</Style.SubTitle>
}

Feature.Text = ({ children }) => {
    return <Style.Text>{children}</Style.Text>
}

Feature.Break = () => {
    return <Style.Break />
}

export default Feature;