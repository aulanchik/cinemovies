import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    flex-direction: column;
    border-bottom: 8px solid #222;
    text-align: center;
    padding: 200px 45px;
`;

const Title = styled.h1`
    max-width: 800px;
    font-size: 4rem;
    font-weight: 500;
    color: #ffffff;
    margin: 0 auto;

    @media (max-width: 600px) {
        font-size: 2.5rem;
    }
`;

const SubTitle = styled.h2`
    color: #ffffff;
    font-size: 1.5rem;
    font-weight: 400;
    margin: 16px auto;

    @media (max-width: 600px) {
        font-size: 18px;
    }
`;

const Text = styled.p`
    color: #ffffff;
    padding: 12px;
    line-height: 1.1;
    font-size: 1.25rem;
    text-align: center;
    font-weight: 400;
    margin: 0 auto;

    @media(max-width: 600px) {
        font-size: 16px;
        line-height: 22px;
    }
`;

const Break = styled.div`
    flex-basis: 100%;
    height: 100%;
`;


export {
    Break,
    Text,
    SubTitle,
    Title,
    Container
}