import React from 'react'
import * as Style from './styles/footer'

const Footer = ({ children, ...props }) => {
    return <Style.Container {...props}>{children}</Style.Container>
}

Footer.Row = ({ children, ...props }) => {
    return <Style.Row {...props}>{children}</Style.Row>;
}

Footer.Column = ({ children, ...props }) => {
    return <Style.Column {...props}>{children}</Style.Column>;
}

Footer.Link = ({ children, ...props }) => {
    return <Style.Link {...props}>{children}</Style.Link>;
}

Footer.Title = ({ children, ...props }) => {
    return <Style.Title {...props}>{children}</Style.Title>;
}

Footer.Text = ({ children, ...props }) => {
    return <Style.Text {...props}>{children}</Style.Text>;
}

Footer.Break = ({ ...props }) => {
    return <Style.Break {...props} />;
}

export default Footer;