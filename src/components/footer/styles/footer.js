import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    max-width: 1000px;
    flex-direction: column;
    padding: 70px 56px;
    margin: auto;
`;

const Column = styled.div`
    display: flex;
    flex-direction: column;
    text-align: left;
`;

const Row = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(175px, 1fr));
    grid-gap: 60px;

    @media (max-width: 1000px) {
        grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
        grid-gap: 20px;
    }
`;

const Link = styled.a`
    color: #757575;
    margin-bottom: 20px;
    font-size: 13px;
    text-decoration: none;
`;

const Title = styled.p`
    font-size: 16px;
    color: #757575;
    margin-bottom: 40px;
`;

const Text = styled.p`
    margin-bottom: 25px;
    font-size: 13px;
    color: #757575;
`;

const Break = styled.div`
    flex-basis: 100%;
    height: 0;
`;

export {
    Container,
    Column,
    Row,
    Link,
    Title,
    Text,
    Break
}