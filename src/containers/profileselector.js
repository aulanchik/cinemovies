import React from "react";
import { Profiles } from "@components";

const ProfileSelector = ({ user, setProfile }) => {
  return (
    <React.Fragment>
      <Profiles>
        <Profiles.Title>Who's watching?</Profiles.Title>
        <Profiles.List>
          <Profiles.User
            onClick={() =>
              setProfile({
                displayName: user.displayName,
                displaySurname: user.displaySurname,
                photoURL: user.photoURL,
              })
            }
          >
            <Profiles.Picture src={user.photoURL} />
            <Profiles.Text>
              {`${user.displayName} ${user.displaySurname}`}
            </Profiles.Text>
          </Profiles.User>
        </Profiles.List>
      </Profiles>
    </React.Fragment>
  );
};

export default ProfileSelector;
