import React from 'react'
import { Accordion, OptForm } from '@components'
import data from '../seeds/static/faqs.json'

const FAQContainer = () => {
    return (
        <Accordion>
            <Accordion.Title>Frequently Asked Questions</Accordion.Title>
            <Accordion.Frame>
                {data.map((item) => (
                    <Accordion.Item key={item.id}>
                        <Accordion.Header>{item.header}</Accordion.Header>
                        <Accordion.Body>{item.body}</Accordion.Body>
                    </Accordion.Item>
                ))}
            </Accordion.Frame>

            <OptForm>
                <OptForm.Text>
                    Ready to watch? Enter your email to create or restart your membership.
                </OptForm.Text>
                <OptForm.Input placeholder="Email Address" />
                <OptForm.Button>Try 30 days free &gt;</OptForm.Button>
            </OptForm>

        </Accordion>
    )
}

export default FAQContainer;
