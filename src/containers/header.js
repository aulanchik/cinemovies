import React from 'react'
import { Header } from '@components'
import * as ROUTES from '@pages/routes'

const HeaderContainer = ({ children }) => {
    return (
        <Header>
            <Header.Frame>
                <Header.Logo location={ROUTES.HOME}/>
                <Header.Button location={ROUTES.SIGNIN}>Sign In</Header.Button>
            </Header.Frame>
            {children}
        </Header>   
    )
}

export default HeaderContainer;
