import React from "react";
import { Header } from "@components";
import ProfileSelector from "./profileselector";
// import FirebaseContext from "../contexts/FirebaseContext";
import Footer from "./footer";

const BrowseContainer = () => {
  const [profile, setProfile] = React.useState({});
  const [category, setCategory] = React.useState("series");

  // const [loading, setLoading] = React.useState(true);

  // const { firebase } = React.useContext(FirebaseContext);

  const user = {
    displayName: "John",
    displaySurname: "Carmack",
    photoURL: "1",
  };

  return profile.displayName ? (
    <React.Fragment>
      <Header src="joker1">
        <Header.Frame>
          <Header.Group>
            <Header.Link
              active={category === "series" ? "true" : "false"}
              onClick={() => setCategory("series")}
            >
              Series
            </Header.Link>
            <Header.Link
              active={category === "films" ? "true" : "false"}
              onClick={() => setCategory("films")}
            >
              Films
            </Header.Link>
          </Header.Group>
        </Header.Frame>

        <Header.Feature>
          <Header.FeaturePromo>Watch Joker Now</Header.FeaturePromo>
            <Header.Text>
              Forever alone in a crowd, failed comedian Arthur Fleck seeks
              connection as he walks the streets of Gotham City. Arthur wears two
              masks -- the one he paints for his day job as a clown, and the guise
              he projects in a futile attempt to feel like he's part of the world
              around him.
            </Header.Text>
          <Header.PlayButton>Play</Header.PlayButton>
        </Header.Feature>
      </Header>
      <Footer />
    </React.Fragment>
  ) : (
    <React.Fragment>
      <ProfileSelector user={user} setProfile={setProfile} />
    </React.Fragment>
  );
};

export default BrowseContainer;
