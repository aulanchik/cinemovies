import React from 'react';
import ReactDOM from 'react-dom';
import GlobalStyle from './wrappers/global'
import FirebaseContext from './contexts/FirebaseContext'
import firebaseConfig from './firebase'
import App from './App';
import 'normalize.css';

ReactDOM.render(
  <FirebaseContext.Provider value={{firebase: firebaseConfig}}>
    <React.Fragment>
      <GlobalStyle />
      <App />
    </React.Fragment>
  </FirebaseContext.Provider>,
  document.getElementById('root')
);
