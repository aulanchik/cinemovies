import React from 'react'
import { Form } from '@components'
import Header from '@containers/header'
import Footer from '@containers/footer'
import { useHistory } from 'react-router-dom'
import * as ROUTES from '@pages/routes'
import FirebaseContext from '@contexts/FirebaseContext'


const SignUp = () => {
    const history = useHistory();
    const { firebase } = React.useContext(FirebaseContext);
    const [firstName, setFirstName] = React.useState('');
    const [emailAddress, setEmailAddress] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [error, setError ] = React.useState('');

    const isInvalid = firstName === '' || password === '' || emailAddress === '';
    
    const handleSignup = (event) => {
        event.preventDefault();
        firebase
            .auth()
            .createUserWithEmailAndPassword(emailAddress, password)
            .then((result) => result.user
                .updateProfile({
                    displayName: firstName,
                    //photoURL will be containing random number from 1 to 5 representing user avatar
                    photoURL: Math.floor(Math.random() * 5) + 1,
                })
                .then(() => {
                    setEmailAddress('');
                    setPassword('');
                    setError('');
                    history.push(ROUTES.BROWSE, {});
                })
            ).catch(err => setError(err.message));
    }
    
    return (
        <React.Fragment>
            <Header>
                <Form>
                    <Form.Title>Sign Up</Form.Title>
                    {error && <Form.Error>{error}</Form.Error>}
                    <Form.Wrapper onSubmit={handleSignup} method="POST">
                        <Form.Input
                            placeholder="First Name"
                            value={firstName}
                            onChange={({ target }) => setFirstName(target.value)}
                        />
                        <Form.Input
                            type="email"
                            placeholder="Email Address"
                            value={emailAddress}
                            onChange={({ target }) => setEmailAddress(target.value)}
                        />
                        <Form.Input
                            type="password"
                            value={password}
                            autoComplete="off"
                            placeholder="Password"
                            onChange={({ target }) => setPassword(target.value)}
                        />
                        <Form.Submit type="submit" disabled={isInvalid}>
                            Sign Up
                        </Form.Submit>
                    </Form.Wrapper>
                </Form>
            </Header>
            <Footer />
        </React.Fragment>
    )
}

export default SignUp;
