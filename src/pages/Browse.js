import React from 'react'
import BrowseContainer from '@containers/browse';

const Browse = () => {
    return (
        <BrowseContainer/>
    )
}

export default Browse;
