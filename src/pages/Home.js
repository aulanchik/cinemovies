import React from 'react'
import { Feature, OptForm } from '@components'
import HeaderContainer from '@containers/header';
import JumbotronContainer from '@containers/jumbotron';
import FAQContainer from '@containers/faq';
import FooterContainer from '@containers/footer';

const Home = () => {
    return (
        <React.Fragment>
            <HeaderContainer>
                <Feature>
                    <Feature.Title>Unlimited movies, TV shows, and more.</Feature.Title>
                    <Feature.SubTitle>Watch anywhere. Cancel anytime.</Feature.SubTitle>
                    <OptForm>
                        <OptForm.Input placeholder="Email Address" />
                        <OptForm.Button>Try 30 days free &gt;</OptForm.Button>
                        <OptForm.Break />
                        <OptForm.Text>Only new members are eligible for this offer.</OptForm.Text>
                    </OptForm>
                </Feature>
            </HeaderContainer>
            <JumbotronContainer />
            <FAQContainer />
            <FooterContainer />
        </React.Fragment>
    )
}

export default Home;
