import React, { useContext } from "react";
import Header from "@containers/header";
import Footer from "@containers/footer";
import { useHistory } from "react-router-dom";
import FirebaseContext from "@contexts/FirebaseContext";
import * as ROUTES from "./routes";
import { Form } from "@components";

const SignIn = () => {
  const history = useHistory();
  const { firebase } = useContext(FirebaseContext);
  const [emailAddress, setEmailAddress] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [error, setError] = React.useState("");

  const handleSignIn = (evt) => {
    evt.preventDefault();

    firebase
      .auth()
      .signInWithEmailAndPassword(emailAddress, password)
      .then(() => {
        setEmailAddress("");
        setPassword("");
        setError("");
        history.push(ROUTES.BROWSE);
      })
      .catch((error) => {
        // console.log('e:', error);
        setError(error.message);
      });
  };

  const isInvalid = (emailAddress === "") | (password === "");

  return (
    <React.Fragment>
      <Header>
        <Form>
          <Form.Title>Sign In</Form.Title>
          {error && <Form.Error>Incorrect credentials</Form.Error>}
          <Form.Wrapper onSubmit={handleSignIn} method="POST">
            <Form.Input
              placeholder="Email address"
              onChange={({ target }) => setEmailAddress(target.value)}
            />
            <Form.Input
              type="password"
              placeholder="Password"
              autoComplete="off"
              onChange={({ target }) => setPassword(target.value)}
            />
            <Form.Submit disabled={isInvalid}>Sign In</Form.Submit>
            <Form.SubTitle>
              You dont have an account?{" "}
              <Form.Link to={ROUTES.SIGNUP}>Sign up now!</Form.Link>
            </Form.SubTitle>
          </Form.Wrapper>
        </Form>
      </Header>
      <Footer />
    </React.Fragment>
  );
};

export default SignIn;
