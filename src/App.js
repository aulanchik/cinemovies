import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Home, SignIn, SignUp, Browse } from '@pages'
import * as ROUTES from '@pages/routes';
 
const App = () => {
  return (
    <Router>
      <Switch>
        <Route path={ROUTES.SIGNIN}>
          <SignIn/>
        </Route>
        <Route path={ROUTES.SIGNUP}>
          <SignUp/>
        </Route>
        <Route path={ROUTES.BROWSE}>
          <Browse/>
        </Route>
        <Route path={ROUTES.HOME}>
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
