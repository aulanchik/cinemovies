const path = require("path");

module.exports = {
  webpack: {
    alias: {
      "@components": path.resolve(__dirname, "src/components/"),
      "@containers": path.resolve(__dirname, "src/containers/"),
      "@contexts": path.resolve(__dirname, "src/contexts"),
      "@seeds": path.resolve(__dirname, "src/seeds"),
      "@pages": path.resolve(__dirname, "src/pages"),
    },
  },
};
